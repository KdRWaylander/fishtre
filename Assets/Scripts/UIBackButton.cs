﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBackButton : MonoBehaviour
{
    private UIQuitButton m_UIQuiButton;

    private void Awake()
    {
        m_UIQuiButton = GetComponentInChildren<UIQuitButton>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_UIQuiButton.Quit();
        }
    }
}