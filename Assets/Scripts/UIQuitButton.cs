﻿using System.Collections;
using UnityEngine;

public class UIQuitButton : MonoBehaviour
{
    [SerializeField] private float m_WaitTime = 3f;
    [SerializeField] private GameObject m_ConfirmationText;

    private bool m_Quit = false;

    public void Quit()
    {
        if (m_Quit)
        {
            Application.Quit();
        }
        else
        {
            StartCoroutine(QuitAction());
        }
    }

    private IEnumerator QuitAction()
    {
        m_Quit = true;
        m_ConfirmationText.SetActive(m_Quit);

        yield return new WaitForSeconds(m_WaitTime);

        m_Quit = false;
        m_ConfirmationText.SetActive(m_Quit);
    }
}