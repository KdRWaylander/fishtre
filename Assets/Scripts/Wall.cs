﻿using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] private float m_BounceVelocityMultiplier = 1.01f;
    [SerializeField] private float m_MaxSpeed = 4f;

    [SerializeField] private SpikeRow m_ThisSpikeRow;
    [SerializeField] private SpikeRow m_OppositeSpikeRow;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.GameState != GameState.Play)
            return;

        if (col.GetComponentInParent<Fish>() != null)
        {
            m_ThisSpikeRow.HideSpikes();
            m_OppositeSpikeRow.SetSpikes();

            GameManager.Instance.IncrementScore();

            Vector2 vel = col.GetComponentInParent<Rigidbody2D>().velocity;
            float newVelX = (Mathf.Abs(vel.x) >= m_MaxSpeed) ? Mathf.Sign(vel.x) * m_MaxSpeed : m_BounceVelocityMultiplier * vel.x;
            col.GetComponentInParent<Rigidbody2D>().velocity = new Vector2(-newVelX, vel.y);

            col.transform.parent.transform.GetComponentInChildren<SpriteRenderer>().flipX = !col.transform.parent.transform.GetComponentInChildren<SpriteRenderer>().flipX;
            Vector3 bubblesPos = col.transform.parent.transform.GetComponentInChildren<ParticleSystem>().gameObject.transform.localPosition;
            col.transform.parent.transform.GetComponentInChildren<ParticleSystem>().gameObject.transform.localPosition = new Vector3(-bubblesPos.x, 0, 0);
        }
    }
}