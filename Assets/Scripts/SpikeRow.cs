﻿using UnityEngine;

public class SpikeRow : MonoBehaviour
{
    [SerializeField] public float m_Threshold = .5f;
    [SerializeField] private float m_ThresholdStep = .01f;
    [SerializeField] private int m_MinSpikesNumber = 2;
    [SerializeField] private int m_MinHolesNumber = 3;

    public void SetSpikes()
    {
        int spikesUp = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (Random.value >= m_Threshold)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
            else
            {
                transform.GetChild(i).gameObject.SetActive(true);
                spikesUp += 1;
            }
        }

        if(spikesUp < m_MinSpikesNumber)
        {
            int i = Random.Range(0, transform.childCount);
            transform.GetChild(i).gameObject.SetActive(true);

            int j = Random.Range(0, transform.childCount);
            while(i == j)
            {
                j = Random.Range(0, transform.childCount);
            }
            transform.GetChild(j).gameObject.SetActive(true);
        }

        if (spikesUp > transform.childCount - m_MinHolesNumber)
        {
            int i = Random.Range(0, transform.childCount);
            transform.GetChild(i).gameObject.SetActive(false);

            int j = Random.Range(0, transform.childCount);
            while (i == j)
            {
                j = Random.Range(0, transform.childCount);
            }
            transform.GetChild(j).gameObject.SetActive(false);

            int k = Random.Range(0, transform.childCount);
            while (i == k || j == k)
            {
                k = Random.Range(0, transform.childCount);
            }
            transform.GetChild(k).gameObject.SetActive(false);
        }

        m_Threshold += m_ThresholdStep;
    }

    public void HideSpikes()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}