﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class UIAudioButton : MonoBehaviour
{
    [Header("Sprites")]
    [SerializeField] private Sprite m_AudioOnSprite;
    [SerializeField] private Sprite m_AudioOffSprite;

    [Header("Snapshots")]
    [SerializeField] private AudioMixerSnapshot m_AudioOnSnapShot;
    [SerializeField] private AudioMixerSnapshot m_AudioOffSnapShot;

    private Image m_Image;

    private const string m_AudioKey = "AUDIO";

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey(m_AudioKey) == false)
            PlayerPrefs.SetInt(m_AudioKey, 1);

        ChangeAudio();
    }

    public void ToggleAudio()
    {
        int audio = (PlayerPrefs.GetInt(m_AudioKey) + 1) % 2;
        PlayerPrefs.SetInt(m_AudioKey, audio);

        ChangeAudio();
    }

    private void ChangeAudio()
    {
        if (PlayerPrefs.GetInt(m_AudioKey) >= 1)
        {
            m_AudioOnSnapShot.TransitionTo(0f);
            m_Image.sprite = m_AudioOnSprite;
        }
        else
        {
            m_AudioOffSnapShot.TransitionTo(0f);
            m_Image.sprite = m_AudioOffSprite;
        }
    }
}