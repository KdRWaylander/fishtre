﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIFishSkinButton : MonoBehaviour
{
    [SerializeField] private UISkinButton m_UISkinButton;
    [SerializeField] private int m_Index;
    [SerializeField] private int m_UnlockThreshold;

    private const string m_HighscoreKey = "HIGHSCORE";
    private const string m_SkinKey = "SKIN";

    private void Start()
    {
        if (PlayerPrefs.GetInt(m_HighscoreKey) < m_UnlockThreshold)
        {
            GetComponent<Button>().interactable = false;
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = m_UnlockThreshold.ToString();
        }
    }

    public void ChangeFishSkin()
    {
        GameManager.Instance.Fish.ChangeSkin(m_Index);
        PlayerPrefs.SetInt(m_SkinKey, m_Index);

        m_UISkinButton.ToggleSkinCanvas();
    }
}