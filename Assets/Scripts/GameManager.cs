﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public GameState GameState { get; set; }
    public int Score { get; private set; }
    public Fish Fish { get { return m_Fish; } }

    [SerializeField] private Fish m_Fish;
    [SerializeField] private float m_TimeBeforeRestart = 1.5f;
    [SerializeField] private TextMeshProUGUI m_ScoreText;
    [SerializeField] private TextMeshProUGUI m_StartText;
    [SerializeField] private TextMeshProUGUI m_RestartText;
    [SerializeField] private TextMeshProUGUI m_HighscoreText;

    private bool m_ReadyToRestart = false;
    private const string m_HighscoreKey = "HIGHSCORE";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GameState = GameState.PreGame;

        if(PlayerPrefs.HasKey(m_HighscoreKey) && PlayerPrefs.GetInt(m_HighscoreKey) > 0)
        {
            m_HighscoreText.text = "Highscore: " + PlayerPrefs.GetInt(m_HighscoreKey).ToString();
        }
        else
        {
            m_HighscoreText.text = "";
            PlayerPrefs.SetInt(m_HighscoreKey, 0);
        }
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(0))
            return;

        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Space) && GameState == GameState.EndGame && m_ReadyToRestart)
        {
            RestartGame();
        }

        #else
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began && GameState == GameState.EndGame && m_ReadyToRestart)
            {
                RestartGame();
            }
        }
        #endif
    }

    public void StartGame()
    {
        GameState = GameState.Play;
        Score = 0;
        m_ScoreText.text = Score.ToString();
        m_StartText.gameObject.SetActive(false);
        m_HighscoreText.gameObject.SetActive(false);
    }

    public void IncrementScore()
    {
        Score += 1;
        m_ScoreText.fontSize = 106;
        m_ScoreText.text = Score.ToString();
    }

    public IEnumerator EndGame()
    {
        GameState = GameState.EndGame;
        if(Score > PlayerPrefs.GetInt(m_HighscoreKey))
        {
            PlayerPrefs.SetInt(m_HighscoreKey, Score);
            m_HighscoreText.gameObject.SetActive(true);
            m_HighscoreText.text = "New highscore !";
        }

        yield return new WaitForSeconds(m_TimeBeforeRestart);
        m_ReadyToRestart = true;
        m_RestartText.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

[System.Serializable]
public enum GameState
{
    PreGame,
    Play,
    Pause,
    EndGame,
    Skin
}