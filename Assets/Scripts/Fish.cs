﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class Fish : MonoBehaviour
{
    [SerializeField] private float m_MoveSpeed;
    [SerializeField] private float m_JumpForce;
    [SerializeField] private Sprite m_FishSkeleton;
    [SerializeField] private float m_TimeToBubbles = 2f;
    [SerializeField] private ParticleSystem m_Bubbles;
    [SerializeField] private AudioClip m_PopSound;
    [SerializeField] private AudioClip m_DeathSound;
    [SerializeField] private Sprite[] m_FishSkins;
    [SerializeField] private Sprite[] m_SkeletonSkins;
    [SerializeField] private PolygonCollider2D[] m_PolygonCollider2Ds;

    private float m_ElapsedTime;

    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody;
    private AudioSource m_AudioSource;

    private const string m_SkinKey = "SKIN";

    private void Awake()
    {
        m_SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey(m_SkinKey) == false)
            PlayerPrefs.SetInt(m_SkinKey, 0);

        ChangeSkin(PlayerPrefs.GetInt(m_SkinKey));

        m_Rigidbody.Sleep();
        m_Bubbles.Play();
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(0))
            return;

        if (GameManager.Instance.GameState == GameState.EndGame)
            return;

        if(GameManager.Instance.GameState != GameState.EndGame)
        {
            m_ElapsedTime += Time.deltaTime;
            if(m_ElapsedTime >= m_TimeToBubbles)
            {
                m_ElapsedTime = 0;
                m_Bubbles.Play();
            }
        }

        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Space) && GameManager.Instance.GameState == GameState.PreGame)
        {
            GameManager.Instance.StartGame();
            m_Rigidbody.WakeUp();
            m_Rigidbody.velocity = m_MoveSpeed * Vector3.right;
            m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
            m_AudioSource.PlayOneShot(m_PopSound);
            return;
        }

        if (Input.GetKeyUp(KeyCode.Space) && GameManager.Instance.GameState == GameState.Pause)
        {
            GameManager.Instance.GameState = GameState.Play;
            Time.timeScale = 1;
            m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, 0);
            m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
            m_AudioSource.PlayOneShot(m_PopSound);
            return;
        }

        if (Input.GetKeyUp(KeyCode.Space) && GameManager.Instance.GameState == GameState.Play)
        {
            m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, 0);
            m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
            m_AudioSource.PlayOneShot(m_PopSound);
        }

        #else
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began && GameManager.Instance.GameState == GameState.PreGame)
            {
                GameManager.Instance.StartGame();
                m_Rigidbody.WakeUp();
                m_Rigidbody.velocity = m_MoveSpeed * Vector3.right;
                m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
                m_AudioSource.PlayOneShot(m_PopSound);
                return;
            }

            if (touch.phase == TouchPhase.Began && GameManager.Instance.GameState == GameState.Pause)
            {
                GameManager.Instance.GameState = GameState.Play;
                Time.timeScale = 1;
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, 0);
                m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
                m_AudioSource.PlayOneShot(m_PopSound);
                return;
            }

            if (touch.phase == TouchPhase.Began && GameManager.Instance.GameState == GameState.Play)
            {
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, 0);
                m_Rigidbody.AddForce(m_JumpForce * Vector2.up, ForceMode2D.Impulse);
                m_AudioSource.PlayOneShot(m_PopSound);
            }
        }
        #endif
    }

    public void ChangeSkin(int index)
    {
        m_SpriteRenderer.sprite = m_FishSkins[index];
        m_FishSkeleton = m_SkeletonSkins[index];

        foreach (PolygonCollider2D collider in m_PolygonCollider2Ds)
        {
            collider.gameObject.SetActive(false);
        }
        m_PolygonCollider2Ds[index].gameObject.SetActive(true);
    }

    public void KillFish()
    {
        m_SpriteRenderer.sprite = m_FishSkeleton;
        m_Bubbles.Play();
        m_AudioSource.PlayOneShot(m_DeathSound);
    }
}