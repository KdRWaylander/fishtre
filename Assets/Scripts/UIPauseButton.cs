﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseButton : MonoBehaviour
{
    public void Pause()
    {
        if (GameManager.Instance.GameState == GameState.Play)
        {
            GameManager.Instance.GameState = GameState.Pause;
            Time.timeScale = 0;
            return;
        }

        if (GameManager.Instance.GameState == GameState.Pause)
        {
            GameManager.Instance.GameState = GameState.Play;
            Time.timeScale = 1;
            return;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause && GameManager.Instance.GameState == GameState.Play)
        {
            Pause();
        }
    }
}