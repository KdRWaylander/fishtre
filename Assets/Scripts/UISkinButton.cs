﻿using TMPro;
using UnityEngine;

public class UISkinButton : MonoBehaviour
{
    [SerializeField] private GameObject m_SkinCanvas;
    [SerializeField] private Fish m_Fish;
    [SerializeField] private TextMeshProUGUI m_StartText;

    public void ToggleSkinCanvas()
    {
        if (GameManager.Instance.GameState == GameState.PreGame || GameManager.Instance.GameState == GameState.Skin)
        {
            bool isUp = m_SkinCanvas.activeSelf;

            m_SkinCanvas.SetActive(!isUp);
            m_Fish.gameObject.SetActive(isUp);
            if (isUp)
            {
                m_Fish.GetComponent<Rigidbody2D>().Sleep();
            }
            m_StartText.gameObject.SetActive(isUp);

            GameManager.Instance.GameState = (GameManager.Instance.GameState == GameState.PreGame) ? GameState.Skin : GameState.PreGame;
        }
    }
}