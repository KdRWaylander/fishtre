﻿using UnityEngine;

public class Spike : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.GetComponentInParent<Fish>() != null && GameManager.Instance.GameState == GameState.Play)
        {
            col.transform.GetComponentInParent<Fish>().KillFish();
            StartCoroutine(GameManager.Instance.EndGame());
        }
    }
}